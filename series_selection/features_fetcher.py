import os
import glob
import dicom
import pandas as pd
import collections
import logging


logger = logging.getLogger('../logs_and_outputs/series_selection')


class FeaturesFetcher(object):

    def __init__(self, dicom_features_to_fetch):
        self.dicom_features_to_fetch = dicom_features_to_fetch

    def fetch_dicom_data(self, series_path):
        all_dicoms_for_series = glob.iglob(os.path.join(series_path, "*.dcm"))
        all_dicoms_features = pd.DataFrame(columns=self.dicom_features_to_fetch)
        for dicom_path in all_dicoms_for_series:
            current_series_features = self._get_features_from_series_dicoms(dicom_path, self.dicom_features_to_fetch)
            all_dicoms_features = all_dicoms_features.append(current_series_features, ignore_index=True)
        if 'InstanceNumber' in self.dicom_features_to_fetch:
            return all_dicoms_features.sort_values('InstanceNumber', ascending=1)
        else:
            return all_dicoms_features

    def _get_features_from_series_dicoms(self, dicom_path, dicom_features_to_fetch):
        dicom_slice = dicom.read_file(dicom_path)
        dicom_slice_row = {}
        for feature_name in dicom_features_to_fetch:
            try:
                feature_value_from_dicom = self._get_feature_value_from_dicom(dicom_slice, feature_name)
                if isinstance(feature_value_from_dicom, collections.Hashable):
                    dicom_slice_row[feature_name] = feature_value_from_dicom
                else:
                    dicom_slice_row[feature_name] = tuple(feature_value_from_dicom)
            except AttributeError:
                logger.info("dicom file does not have {} field | {}".format(feature_name, dicom_path))
                dicom_slice_row[feature_name] = None
        return dicom_slice_row

    def _get_feature_value_from_dicom(self, dicom_slice, feature_name):
        return getattr(dicom_slice, feature_name)



