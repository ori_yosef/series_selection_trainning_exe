import argparse
import pandas as pd
from series_selection import files_io
import logging

logger = logging.getLogger('../logs_and_outputs/series_selection')


class SeriesPrioritizer:

    def __init__(self, config_file_path):
        configuration_for_series_selection = files_io.read_config_file(config_file_path)
        self.minimum_criteria_dict = self._get_minimum_criteria_dict(configuration_for_series_selection)
        self.best_criteria_dict = self._get_best_criteria_dict(configuration_for_series_selection)
        self.prioritization_criteria_keys_increase_list = self._get_prioritization_criteria_dict(configuration_for_series_selection)
        return

    def _get_minimum_criteria_dict(self, configuration_for_series_selection):
        minimum_criteria_dict = self._get_criteria('minimum_criteria', configuration_for_series_selection)
        return minimum_criteria_dict

    def _get_prioritization_criteria_dict(self, configuration_for_series_selection):
        prioritization_criteria_dict = self._get_criteria('prioritization_criteria', configuration_for_series_selection)
        prioritization_criteria_keys_increase_list = self._fix_criteria_dict(prioritization_criteria_dict)
        return prioritization_criteria_keys_increase_list

    def _get_best_criteria_dict(self, configuration_for_series_selection):
        best_criteria_dict = self._get_criteria('best_criteria', configuration_for_series_selection)
        return best_criteria_dict

    def prioritize_series(self, series_features, output_path=None):
        if not isinstance(series_features, pd.DataFrame):
            series_features = pd.read_excel(series_features, sheet_name='Sheet1')
        series_features = self._remove_series_without_minimum_criteria(series_features)
        series_features = self._sort_series_by_prioritization_criteria(series_features)
        series_features = self._sort_by_best_criteria(series_features)
        if output_path is None:
            return series_features
        files_io.save_dataframe(series_features, output_path)
        return series_features

    def _remove_series_without_minimum_criteria(self, series_features):
        for feature_name in self.minimum_criteria_dict:
            feature_minimum_value = self.minimum_criteria_dict[feature_name]
            feature_mask = series_features[feature_name].apply(str) != feature_minimum_value
            series_features.drop(series_features[feature_mask].index, inplace=True)
        return series_features

    def _sort_series_by_prioritization_criteria(self, series_features):
        return series_features.sort_values(self.prioritization_criteria_keys_increase_list[0],
                                           ascending=self.prioritization_criteria_keys_increase_list[1])

    def _sort_by_best_criteria(self, series_features, margin=0.2):
        if series_features.empty:
            return series_features
        series_features['best_criteria'] = 1
        for feature_name in self.best_criteria_dict:
            feature_value = self.best_criteria_dict[feature_name]
            feature_mask = (series_features[feature_name] - feature_value).abs() > margin
            series_features.at[feature_mask, 'best_criteria'] = 0
        series_features = series_features.sort_values('best_criteria',
                                                      ascending=0)
        return series_features

    def _fix_criteria_dict(self, prioritization_criteria_dict):
        prioritization_dict = {'descending': False, 'ascending': True}
        return (list(prioritization_criteria_dict.keys()),
                [prioritization_dict[v] for v in prioritization_criteria_dict.values()])

    def _get_criteria(self, criteria_type, configuration):
        if criteria_type in configuration:
            logger.info("{}:{}".format(criteria_type, configuration[criteria_type]))
            return configuration[criteria_type]
        else:
            logger.info("there is no {} to prioritize by in the configuration file".format(criteria_type))
        return {}


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='The module prioritize series from an excel file which'
                                                 'includes series features. The prioritization criteria are '
                                                 'defined in config.json.')
    parser.add_argument('-s', '--series_features_to_prioritize', required=True,
                        help='excel file with series features')
    parser.add_argument('-c', '--config_file_path', required=True,
                        help='path to configuration file where prioritization criteria located')
    parser.add_argument('-p', '--output_path', default='InPlace',
                        help='if provided, series prioritization result will be save in output_path'
                             '(default: series_features_to_prioritize)')
    args = parser.parse_args()
    prioritizer = SeriesPrioritizer(args.config_file_path)
    if args.output_path == 'InPlace':
        args.output_path = args.series_features_to_prioritize
    prioritizer.prioritize_series(args.series_features_to_prioritize, args.output_path)
