import argparse
import glob
import os
import pandas as pd
from series_selection.study_series_selection import StudySeriesSelector
from series_selection import files_io
import logging


logger = logging.getLogger('../logs_and_outputs/series_selection')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('series_selection.log', mode='w')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)


class SeriesSelector(object):

    def __init__(self, config_file_path):
        self.selector = StudySeriesSelector(config_file_path)

    def series_select(self, studies_path, output_path):
        all_studies = glob.iglob(os.path.join(studies_path, "*"))
        all_series_features = pd.DataFrame()
        for study_path in all_studies:
            if not os.path.isdir(study_path):
                logger.info("skip {}".format(study_path))
                continue
            logger.info("series selection on study | {}".format(study_path))
            current_study_series_features = self.selector.series_select(study_path, None)
            all_series_features = pd.concat([all_series_features, current_study_series_features], axis=0, sort=False)
        if output_path is not None:
            files_io.save_dataframe(all_series_features, output_path)
        return


if __name__ == '__main__':
    debug_mode = True
    if debug_mode:
        debug_studies_path = "/media/SSD1/series_selection_exe_DB/"
        debug_config_file_path = "config.json"
        debug_output_path = "../logs_and_outputs/series_selection_test.xlsx"
        selector = SeriesSelector(debug_config_file_path)
        selector.series_select(debug_studies_path, debug_output_path)
    else:
        parser = argparse.ArgumentParser(description='The module extract features from a dicom series and'
                                                     'create data frame for the series with features defines in'
                                                     'config.json.')
        parser.add_argument('-s', '--studies_path', required=True,
                            help='studies head directory')
        parser.add_argument('-c', '--config_file_path', required=True,
                            help='path to configuration file where features to extract and '
                                 'prioritization criteria located')
        parser.add_argument('-p', '--output_path', required=True,
                            help='series prioritization result will be save in output_path')
        args = parser.parse_args()
        selector = SeriesSelector(args.config_file_path)
        selector.series_select(args.studies_path, args.output_path)
