import numpy as np
from metadata_extraction.anatomy.anatomy_type import AnatomyType
from metadata_extraction.metadata_classifier_creator import create_anatomy_classifier
from metadata_extraction.evaluation.predictions_api import predict_on_series_path
from dicomaizer.exceptions import DicomValidationError
import logging


def _convert_keys_values(src_dict):
    return {value: key for key, value in src_dict.items()}


logger = logging.getLogger('../logs_and_outputs/series_selection')
PLANE_TO_AXIS = {'SAG': 0, 'COR': 1, 'AX': 2}
AXIS_TO_PLANE = _convert_keys_values(PLANE_TO_AXIS)
anatomy_classifier = create_anatomy_classifier(AnatomyType.BRAIN)


def get_dicom_features_to_fetch_dict(features_to_extract):
    feature_to_dicom_attrs = {
        'Plane': ['ImageOrientationPatient'],
        'SpacingBetweenSlices': ['InstanceNumber', 'ImagePositionPatient'],
        'NumberOfSlices': ['InstanceNumber'],
        'IsBrain': []
    }
    return {feature_name: feature_to_dicom_attrs.get(feature_name, [feature_name])
            for feature_name in features_to_extract}


def return_feature_value(feature_name, dicoms_data_for_calculation):
    dicoms_data_for_calculation = _mask_illegal_values(dicoms_data_for_calculation)
    if dicoms_data_for_calculation.empty:
        _log_feature_none_result("no value was seen", feature_name)
        return None
    most_seen_feature_value = dicoms_data_for_calculation.value_counts().idxmax()
    return most_seen_feature_value


def return_num_of_slices_feature(all_seen_uids_feature):
    return len(all_seen_uids_feature)


def _mask_illegal_values(dicoms_data_to_mask):
    legal_values_mask = (dicoms_data_to_mask.notnull()) & (dicoms_data_to_mask != '')
    return dicoms_data_to_mask[legal_values_mask]


def return_slice_thickness_feature(all_seen_slice_thickness, slice_thickness_max_std):
    all_seen_slice_thickness = _mask_first_and_last_values(all_seen_slice_thickness)
    all_seen_slice_thickness = _mask_illegal_values(all_seen_slice_thickness)
    if all_seen_slice_thickness.empty:
        _log_feature_none_result("slice_thickness is N/A", "SliceThickness")
        return None
    all_seen_slice_thickness_mean = all_seen_slice_thickness.mean() + np.finfo(float).eps
    thickness_std_divided_mean = abs(all_seen_slice_thickness.std()/all_seen_slice_thickness_mean)
    if thickness_std_divided_mean <= slice_thickness_max_std:
        most_seen_thickness_of_series = all_seen_slice_thickness.value_counts().idxmax()
        return most_seen_thickness_of_series
    else:
        _log_feature_none_result("slice_thickness is not uniform", "SliceThickness")
        return None


def _mask_first_and_last_values(df):
    return df.iloc[1:-1]


def return_slice_spacing_feature(all_seen_patient_position, series_plane):
    all_seen_patient_position = _mask_first_and_last_values(all_seen_patient_position)
    if len(all_seen_patient_position) < 2:
        _log_feature_none_result("patient position N/A", "SpacingBetweenSlices")
        return None
    elif series_plane not in PLANE_TO_AXIS:
        _log_feature_none_result("patient position plane is UNKNOWN", "SpacingBetweenSlices")
        return None
    else:
        return _calculate_slice_spacing(all_seen_patient_position, series_plane)


def _calculate_slice_spacing(all_seen_patient_position, series_plane):
    two_random_slices_from_series = all_seen_patient_position.sample(n=2)
    slice_1_instance_number, slice_1_image_position_patient = _get_details_from_slice(
        two_random_slices_from_series.iloc[0])
    slice_2_instance_number, slice_2_image_position_patient = _get_details_from_slice(
        two_random_slices_from_series.iloc[1])
    num_of_slices_diff = _calculate_slices_diff(slice_1_instance_number, slice_2_instance_number)
    if num_of_slices_diff == 0:
        _log_feature_none_result("InstanceNumber is constant", "SpacingBetweenSlices")
        return None
    patient_position_diff = _calculate_positions_diff(slice_1_image_position_patient,
                                                      slice_2_image_position_patient,
                                                      PLANE_TO_AXIS[series_plane])
    return patient_position_diff/num_of_slices_diff


def _get_details_from_slice(slice_details):
    slice_instance_number = slice_details['InstanceNumber']
    slice_image_position_patient = slice_details['ImagePositionPatient']
    return slice_instance_number, slice_image_position_patient


def _calculate_positions_diff(slice_1_image_position_patient, slice_2_image_position_patient, axis):
    slice_1_position = slice_1_image_position_patient[axis]
    slice_2_position = slice_2_image_position_patient[axis]
    patient_position_diff = abs(slice_1_position - slice_2_position)
    return patient_position_diff


def _calculate_slices_diff(slice_1_instance_number, slice_2_instance_number):
    return abs(slice_1_instance_number - slice_2_instance_number)


def return_plane_feature(all_seen_plane_vectors):
    all_seen_plane_vectors = _mask_illegal_values(all_seen_plane_vectors)
    if all_seen_plane_vectors.empty:
        _log_feature_none_result("Plane is N/A", "Plane")
        return None
    all_seen_planes = all_seen_plane_vectors.apply(_calculate_plane_from_plane_vector)
    return all_seen_planes.value_counts().idxmax()


def _check_vectors_similarity(v, u, margin=0.2):
    return np.all(np.abs(u - v) < margin)


def _calculate_plane_from_plane_vector(image_position_patient):
    abs_normalized_plane_normal = _get_plane_normal(image_position_patient)
    is_series_plane_on_axis = np.full(3, True, dtype=bool)
    for axis in AXIS_TO_PLANE:
        axis_normal = np.zeros(len(AXIS_TO_PLANE))
        axis_normal[axis] = 1
        is_series_plane_on_axis[axis] = _check_vectors_similarity(axis_normal, abs_normalized_plane_normal)
    if sum(is_series_plane_on_axis) > 1 or not any(is_series_plane_on_axis):
        return 'UNKNOWN'
    else:
        found_axis = np.argwhere(is_series_plane_on_axis == True)[0,0]
        return AXIS_TO_PLANE[found_axis]


def _get_plane_normal(image_position_patient):
    if len(image_position_patient) != 6:
        logger.info("image_position_patient in not valid. expected 2 plane vectors | Plane feature set to UKNOWN")
        return np.array([0,0,0])
    first_vector_in_plane = image_position_patient[:3]
    second_vector_in_plane = image_position_patient[-3:]
    plane_normal = np.cross(first_vector_in_plane, second_vector_in_plane)
    plane_normal_norm = np.linalg.norm(plane_normal)
    abs_normalized_plane_normal = np.abs(plane_normal / plane_normal_norm)
    return abs_normalized_plane_normal


def get_is_brain_feature(series_path, series_calculated_instance_uid):
    try:
        is_brain_prediction = predict_on_series_path(
            series_calculated_instance_uid,
            series_path, anatomy_classifier)
    except (DicomValidationError, AttributeError):
        is_brain_prediction = None
    if is_brain_prediction is not None:
        is_brain_decision = list(is_brain_prediction.decision)[0]
        return is_brain_decision == AnatomyType.BRAIN
    else:
        _log_feature_none_result("predict_on_series_path return None", "IsBrain")
        return None


def _log_feature_none_result(feature_name, reason):
    logger.info("{} | {} feature set to None".format(feature_name, reason))