import pandas as pd
from json import load


pd.set_option('display.max_columns', 100)


def read_config_file(config_file_path):
    with open(config_file_path) as config_file:
        return load(config_file)


def save_dataframe(data_frame_for_series, output_path):
    writer = pd.ExcelWriter(output_path, engine='xlsxwriter')
    data_frame_for_series.to_excel(writer, sheet_name='Sheet1', index=False)
    worksheet = writer.sheets['Sheet1']
    for i, width in enumerate(_get_col_widths(data_frame_for_series), start=-1):
        worksheet.set_column(i, i, width)
    writer.save()
    return


def _get_col_widths(dataframe, extra_width=2):
    idx_max = max([len(str(s)) for s in dataframe.index.values] + [len(str(dataframe.index.name))]) + extra_width
    return [idx_max] + [max([len(str(s)) for s in dataframe[col].values] + [len(col)]) + extra_width
                        for col in dataframe.columns]


