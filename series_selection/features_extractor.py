import numpy as np
import argparse
import pandas as pd
from series_selection import calculation_utils, files_io
import logging
import collections
from series_selection.features_fetcher import FeaturesFetcher

logger = logging.getLogger('../logs_and_outputs/series_selection')


class FeatureExtractor(object):

    def __init__(self, config_file_path):
        configuration_for_series_selection = files_io.read_config_file(config_file_path)
        self.slice_thickness_max_std = self._set_slice_thickness_max_std(configuration_for_series_selection)
        self.features_to_extract = self._get_features_to_extract(configuration_for_series_selection)
        self.dicom_features_to_fetch_dict = calculation_utils.get_dicom_features_to_fetch_dict(self.features_to_extract)
        self.dicom_features_to_fetch = self._union_lists(self.dicom_features_to_fetch_dict.values())
        self.featrures_fetcher = FeaturesFetcher(self.dicom_features_to_fetch)

    def extract_features(self, series_path, output_path=None):
        logger.info("series feature extraction | {}".format(series_path))
        all_dicoms_features = self.featrures_fetcher.fetch_dicom_data(series_path)
        series_feature_dict = self._get_series_features_from_dicoms(all_dicoms_features)
        if 'IsBrain' in self.dicom_features_to_fetch_dict:
            series_feature_dict['IsBrain'] = calculation_utils.\
                get_is_brain_feature(series_path, series_feature_dict['SeriesInstanceUID'])
        data_frame_for_series = pd.DataFrame([series_feature_dict])
        if output_path is not None:
            files_io.save_dataframe(data_frame_for_series, output_path)
        return data_frame_for_series

    def _union_lists(self, list_of_strings_and_lists):
        result = sum(list_of_strings_and_lists, [])
        return list(set(result))

    def _get_series_features_from_dicoms(self, all_dicoms_features):
        series_feature_dict = collections.OrderedDict()
        for feature_name in self.features_to_extract:
            dicoms_features_for_feature_calculation = self._return_dicom_features_to_fetch(feature_name)
            dicoms_data_for_calculation = all_dicoms_features[dicoms_features_for_feature_calculation]
            if feature_name == 'NumberOfSlices':
                calculated_feature_value = calculation_utils.return_num_of_slices_feature(dicoms_data_for_calculation)
            elif feature_name == 'SliceThickness':
                calculated_feature_value = calculation_utils.return_slice_thickness_feature(
                    dicoms_data_for_calculation,
                    self.slice_thickness_max_std
                )
            elif feature_name == 'Plane':
                calculated_feature_value = calculation_utils.return_plane_feature(dicoms_data_for_calculation)
            elif feature_name == 'SpacingBetweenSlices':
                if 'Plane' not in series_feature_dict:
                    series_plane = calculation_utils.return_plane_feature(dicoms_data_for_calculation)
                else:
                    series_plane = series_feature_dict['Plane']
                calculated_feature_value = calculation_utils.return_slice_spacing_feature(dicoms_data_for_calculation,
                                                                                          series_plane)
            elif feature_name == 'IsBrain':
                continue
            else:
                calculated_feature_value = calculation_utils.return_feature_value(feature_name,
                                                                                  dicoms_data_for_calculation)
            series_feature_dict[feature_name] = calculated_feature_value
        return series_feature_dict

    def _return_dicom_features_to_fetch(self, feature_name):
        if len(self.dicom_features_to_fetch_dict[feature_name]) == 1:
            return self.dicom_features_to_fetch_dict[feature_name][0]
        return self.dicom_features_to_fetch_dict[feature_name]

    def _get_features_to_extract(self, configuration):
        try:
            return configuration["features"]
        except Exception as e:
            error_msg = "there is no features to extract in config.json"
            logger.error('Error at {}'.format(error_msg), exc_info=e)
            raise Exception(error_msg)


    def _set_slice_thickness_max_std(self, configuration):
        slice_thickness_max_std = configuration.get("slice_thickness_max_std", "ןinf")
        if slice_thickness_max_std == "inf":
            logger.info("there is no slice_thickness_max_std parameter in the configuration file")
            return np.inf
        else:
            return slice_thickness_max_std


if __name__ == '__main__':
    debug_mode = True
    if debug_mode:
        debug_series_path = "/media/SSD1/series_selection_exe_DB/A10026553799/" \
                            "1.2.826.0.1.3680043.9.6883.1.11696540860665768187462737248807881"
        debug_config_file_path = "config.json"
        debug_output_path = "../logs_and_outputs/features_extractor.xlsx"
        extractor = FeatureExtractor(debug_config_file_path)
        extractor.extract_features(debug_series_path, debug_output_path)
    else:
        parser = argparse.ArgumentParser(description='The module extract features from a dicom series and'
                                                     'create data frame for the series with features defines in'
                                                     'config.json.')
        parser.add_argument('-s', '--series_path', required=True,
                            help='series head directory')
        parser.add_argument('-c', '--config_file_path', required=True,
                            help='path to configuration file where features to extract located')
        parser.add_argument('-p', '--output_path', default=None,
                            help='if provided, features extractor result will be save to output_path.xlsx')
        args = parser.parse_args()
        extractor = FeatureExtractor(args.config_file_path)
        extractor.extract_features(args.series_path, args.output_path)
