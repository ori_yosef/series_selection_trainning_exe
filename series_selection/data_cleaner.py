import os
import glob
import shutil
import dicom


def clean_studies_dir(studies_path):
    required_changes = _build_changes_dict(studies_path)
    if not required_changes:
        print("All the dicoms are in the correct path")
    else:
        _apply_changes(required_changes)
    _delete_empty_dirs(studies_path)
    return


def _build_changes_dict(studies_path):
    changes = {}
    all_paths = glob.iglob(os.path.join(studies_path, "**", "*.dcm"), recursive=True)
    for path in all_paths:
        gt_path_for_dicom = _get_GT_path(path, studies_path)
        if path != gt_path_for_dicom:
            changes[path] = gt_path_for_dicom
    return changes


def _apply_changes(required_changes):
    apply_all = False
    for file, destination in required_changes.items():
        if apply_all:
            move_file(file, destination)
        else:
            apply_change = input("required change: {} will move to {}\n".format(file, destination) +
                                 "For applying the change please enter Y, for all please enter ALL: ")
            if apply_change in ['Y', 'ALL']:
                move_file(file, destination)
                print("{} moved to {}".format(file, destination))
            if apply_change == 'ALL':
                apply_all = True
    return


def _delete_empty_dirs(studies_path):
    delete_empty_folders = input("To delete empty folders please enter Y: ")
    if delete_empty_folders == 'Y':
        all_paths = glob.iglob(os.path.join(studies_path, "**"), recursive=True)
        for path in all_paths:
            _clean_empty_dirs(path)
    return


def _clean_empty_dirs(path):
    if os.path.isdir(path) and len(os.listdir(path)) == 0:
        os.rmdir(path)
        _clean_empty_dirs(os.path.dirname(path))
    return


def _get_GT_path(path, studies_path):
    dicom_file_name = os.path.basename(path)
    dicom_study_accession_number, dicom_series_uid = _get_dicom_uids(path)
    GT_path = os.path.join(studies_path, dicom_study_accession_number, dicom_series_uid, dicom_file_name)
    return GT_path


def _get_dicom_uids(dicom_file):
    dicom_slice = dicom.read_file(dicom_file)
    dicom_study_accession_number = dicom_slice.AccessionNumber
    dicom_series_uid = dicom_slice.SeriesInstanceUID
    return dicom_study_accession_number, dicom_series_uid


def move_file(file, destination):
    if os.path.exists(destination):
        os.remove(file)
    else:
        file_dest_dir = os.path.dirname(destination)
        os.makedirs(file_dest_dir, exist_ok=True)
        shutil.move(file, destination)


if __name__ == '__main__':
    clean_studies_dir("/media/SSD1/test_studies")