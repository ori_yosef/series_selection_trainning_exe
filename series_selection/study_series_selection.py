import argparse
import glob
import os
import pandas as pd
from series_selection.series_prioritizer import SeriesPrioritizer
from series_selection.features_extractor import FeatureExtractor


class StudySeriesSelector(object):

    def __init__(self, config_file_path):
        self.extractor = FeatureExtractor(config_file_path)
        self.prioritizer = SeriesPrioritizer(config_file_path)

    def series_select(self, study_path, output_path):
        study_series_data_frame = self._series_combine(study_path)
        study_series_data_frame = self._study_series_prioritize(study_series_data_frame, output_path)
        return study_series_data_frame

    def _series_combine(self, study_path):
        all_series_for_study = glob.iglob(os.path.join(study_path, "*"))
        study_series_features = pd.DataFrame()
        for series_path in all_series_for_study:
            current_series_features = self.extractor.extract_features(series_path, None)
            study_series_features = pd.concat([study_series_features, current_series_features], axis=0, sort=False)
        study_series_features = study_series_features.reset_index(drop=True)
        return study_series_features

    def _study_series_prioritize(self, study_series_features, output_path):
        study_series_features = self.prioritizer.prioritize_series(study_series_features,
                                                                   output_path)
        return study_series_features


if __name__ == '__main__':
    debug_mode = True
    if debug_mode:
        debug_study_path = "/media/SSD1/series_selection_exe_DB/ACC000032987"
        debug_config_file_path = "config.json"
        debug_output_path = "../logs_and_outputs/study_series_selection.xlsx"
        Selector = StudySeriesSelector(debug_config_file_path)
        Selector.series_select(debug_study_path, debug_output_path)
    else:
        parser = argparse.ArgumentParser(description='The module extract features from a dicom series and'
                                                     'create data frame for the series with features defines in'
                                                     'config.json.')
        parser.add_argument('-s', '--study_path', required=True,
                            help='study head directory')
        parser.add_argument('-c', '--config_file_path', required=True,
                            help='path to configuration file where features to extract and '
                                 'prioritization criteria located')
        parser.add_argument('-p', '--output_path',default=None,
                            help='if provided, series prioritization result will be save to '
                                 'output_path.xlsx')
        args = parser.parse_args()
        Selector = StudySeriesSelector(args.config_file_path)
        Selector.series_select(args.study_path, args.output_path)